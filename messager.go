package main

import tgbotapi "gopkg.in/telegram-bot-api.v4"

//MENSAGENS
func mandarMensagem(ChatID int64, Mensagem string, bot *tgbotapi.BotAPI) {
	msg := tgbotapi.NewMessage(ChatID, Mensagem)
	msg.ParseMode = "html"

	bot.Send(msg)
}
func mandarFoto(ChatID int64, UserName string, bot *tgbotapi.BotAPI) {
	msg := tgbotapi.NewPhotoUpload(ChatID, "downloads/imagem.jpg")
	msg.Caption = "@" + UserName

	bot.Send(msg)
}
func mandaAudio(ChatID int64, UserName string, bot *tgbotapi.BotAPI) {
	msg := tgbotapi.NewAudioUpload(ChatID, "downloads/audio.mp3")
	msg.Title = "@" + UserName
	bot.Send(msg)
}

package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"

	_ "github.com/mattn/go-sqlite3"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

const (
	// Rules são Regras para os grupos
	Rules = "config/regras.md"
	// TDV Mural com todos que tentaram user comando para adm
	TDV           = "config/txt_da_vergonha.md"
	tabelaUser    = "usuarios"
	tabelaBanidos = "usuarios_banidos"
)

func main() {
	jsonConfigF := leitorArquivo("config/config.json")
	dec := json.NewDecoder(strings.NewReader(jsonConfigF))
	for {
		if err := dec.Decode(&config); err == io.EOF {
			break
		} else if err != nil {
			logError(err)
		}
	}

	botToken = config.BotToken

	bot, err := iniciaBot()

	logError(err)

	db, err := iniciaDatabase()
	logError(err)

	defer db.Close()

	log.Printf("Logado na conta %s", bot.Self.UserName)
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 5 //Tempo pra atualizar
	logErroRequests := tgbotapi.APIResponse{}

	updates, err := bot.GetUpdatesChan(u)

	for msg := range updates { //Recebe as mensagem
		if msg.Message == nil {
			continue
		}
		//////////////////////////////Verifica o BAN//////////////////
		if msg.Message.NewChatMembers != nil {
			usuario := fmt.Sprintf("%v", msg.Message.NewChatMembers)
			if rUser(*db, string(usuario), tabelaBanidos) == 0 {
			} else {
				log.Println("[-]O usuarios " + usuario + " tentou entrar no grupo!")
				kickUser(rUser(*db, string(usuario), tabelaUser), msg.Message.Chat.ID, bot)
				msg.Message.NewChatMembers = nil
			}
		}

		//Cadastra o Usuario
		if rUser(*db, msg.Message.From.UserName, tabelaUser) != msg.Message.From.ID {
			iUser(*db, msg.Message.From.UserName, msg.Message.From.ID, tabelaUser)
			log.Println("[+]O usuario " + msg.Message.From.UserName + "(ID:" + strconv.Itoa(msg.Message.From.ID) + ") foi cadastrado!")
		}

		if msg.Message.Text != "" {
			log.Printf("[%s] %s", msg.Message.From.UserName, msg.Message.Text)
		}
		if msg.Message.LeftChatMember != nil {
			log.Printf("[%s] foi removido do grupo.", msg.Message.LeftChatMember)
			mandarMensagem(msg.Message.Chat.ID, "<b>Esse deve ter feito besteira....( ͡° ͜ʖ ͡°)</b>", bot)
		}
		if msg.Message.NewChatMembers != nil {
			log.Printf("[%v] foi adicionado/convidado ao grupo por %v!", msg.Message.NewChatMembers, msg.Message.From.UserName)
			mandarMensagem(msg.Message.Chat.ID, Textos["welcome"], bot)
		}

		if msg.Message.Text == "" {
			continue
		} else if VerificaComando(msg.Message.Text) == true {
			Resp := comandos(msg.Message.Text, msg.Message.From.ID, msg.Message.From.UserName, msg.Message.Chat.ID, db, bot, logErroRequests)
			mandarMensagem(msg.Message.Chat.ID, Resp, bot)
		}

	}
}

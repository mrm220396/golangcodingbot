package main

import (
	"database/sql"
	"log"
	"os"

	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

//FUNÇÕES RELACIONADAS AO BOT/SERVIDOR/DATABASE
func iniciaBot() (*tgbotapi.BotAPI, error) {
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_TOKEN"))
	logError(err)

	bot.Debug = false

	return bot, err
}
func iniciaDatabase() (*sql.DB, error) {
	db, err := sql.Open("sqlite3", "./database.db")
	logError(err)

	return db, err
}
func logError(logError error) {
	if logError != nil {
		log.Println("[ERROR]", logError)

	}
}

///////////////////////////////////////////BASE DE DADOS////////////////////////////////////////////////////
func rUser(db sql.DB, username string, tabela string) (idUsuario int) { //Para o @panuto que não entende minha pog mais que esta implicito o que a função faz(olha o comando sql)ele vai puxar pelo nome do usuario o id do telegram que esta salvo no db :)
	rows, err := db.Query("SELECT id_Usuario FROM " + tabela + " WHERE username = '" + username + "'")
	logError(err)
	for rows.Next() {
		err = rows.Scan(&idUsuario)
		logError(err)
	}
	defer rows.Close()
	return idUsuario
}
func iUser(db sql.DB, username string, idUsuario int, tabela string) { //Aqui já insere o usuario
	tx, err := db.Begin()
	logError(err)
	stmt, err := tx.Prepare("insert into " + tabela + "(username,idUsuario) values(?,?)")
	logError(err)
	_, err = stmt.Exec(username, idUsuario)
	logError(err)
	tx.Commit()
}

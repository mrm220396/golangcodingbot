package main

import (
	"fmt"
	"log"
	"strconv"

	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

// User retorna nome e id do usuário
type User struct {
	Name string
	ID   int
}
type jsonConfig struct {
	BotToken string
	User     []User
}

var (
	idUsuario int
	userAdmin string
	config    jsonConfig
	botToken  string
	reiGelado int
	barionix  int
	janMesu   int = 202694844
)

// FUNÇÕES REACIONADAS A PERMISSÕES
func permCheck(idUsuario int) string {
	if idUsuario == reiGelado {
		return "ReiGel_ado"
	} else if idUsuario == barionix {
		return "Barionix"
	} else if idUsuario == janMesu {
		return "Jan Mesu"
	} else {
		return "false"
	}
}
func kickUser(idUsuario int, ChatID int64, bot *tgbotapi.BotAPI) {
	user := tgbotapi.ChatMemberConfig{
		ChatID: ChatID,
		UserID: idUsuario,
	}

	k := tgbotapi.KickChatMemberConfig{
		ChatMemberConfig: user,
	}
	bot.KickChatMember(k)
}
func returnAdmins(ChatID int64, bot *tgbotapi.BotAPI) []tgbotapi.ChatMember {
	k := tgbotapi.ChatConfig{
		ChatID: ChatID,
	}
	structAdmin, err := bot.GetChatAdministrators(k)
	logError(err)

	return structAdmin
}
func txtdavergonhaArquivo(username string, idUsuario int, motivo string) {
	fmt.Println(username)
	txtdaVergonha := leitorArquivo(TDV)
	escreveTxt := txtdaVergonha + "\n-" + username + " - ID:" + strconv.Itoa(idUsuario)
	escreveArquivo(TDV, escreveTxt)
	log.Printf("[-]O usuario " + username + " de ID:" + strconv.Itoa(idUsuario) + " tentou " + motivo + "!")
}
